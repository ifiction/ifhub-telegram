<?php
require_once("vendor/autoload.php");
use Symfony\Component\Yaml\Yaml;
$config = Yaml::parse(file_get_contents('config.yml'));
$client = new \GuzzleHttp\Client();
if (empty($config['AP_CLIENT_ID'])) {
  $req = $client->request('POST', $config['AP_SERVER'] . '/api/v1/apps', [
    'json' => [
      'client_name' => 'RSS Bridge',
      'redirect_uris' => 'urn:ietf:wg:oauth:2.0:oob',
      'scopes' => 'read write',
    ],
    'auth' => [
      'noreply@ifhub.club',
      'MustardEscapableElevatorManpowerUnfitting',
    ],
  ]);
  print_r(json_decode($req->getBody()));
  return;
}
if (empty($config['AP_CODE'])) {
  echo $config['AP_SERVER'].'/oauth/authorize?client_id='.$config['AP_CLIENT_ID'].'&redirect_uri=urn:ietf:wg:oauth:2.0:oob&response_type=code&scope=read+write';
  return;
}
$req = $client->request('POST', $config['AP_SERVER'] . '/oauth/token', [
  'json' => [
    'redirect_uri' => 'urn:ietf:wg:oauth:2.0:oob',
    'client_id' => $config['AP_CLIENT_ID'],
    'client_secret' => $config['AP_SECRET'],
    'grant_type' => 'authorization_code',
    'code' => $config['AP_CODE'],
  ],
  ]);
var_dump(json_decode($req->getBody()));
