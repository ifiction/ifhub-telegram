<?php
require_once("vendor/autoload.php");

use Symfony\Component\Yaml\Yaml;

$config = Yaml::parse(file_get_contents('config.yml'));
$seen = [];
if (file_exists('.seen.yml')) {
  $seen = Yaml::parse(file_get_contents('.seen.yml'));
}

function get_text($url)
{
  $client = new \GuzzleHttp\Client();
  $req = $client->request('GET', $url);
  return (string) $req->getBody();
}
function mastodon_post($text)
{
  global $config;
  if (empty($text)) {
    echo 'Empty post - skipping';
    return;
  }
  $client = new \GuzzleHttp\Client();
  $req = $client->request('GET', $config['AP_SERVER'] . '/api/v1/accounts/verify_credentials', [
    'headers' => [
      'Authorization' => 'Bearer ' . $config['AP_TOKEN'],
    ],
  ]);
  try {
    $client->request('POST', $config['AP_SERVER'] . '/api/v1/statuses', [
      'headers' => [
        'Authorization' => 'Bearer ' . $config['AP_TOKEN'],
      ],
      'form_params' => [
        'status' => $text,
        'language' => 'ru',
        'visibility' => 'public',
      ]
    ]);
  } catch (\Exception $e) {
    echo $e->getMessage() . PHP_EOL;
    echo $e->getTraceAsString();
  }
}

function ellipse($str, $n_chars, $crop_str = '[…]')
{
  $buff = $str;
  if (strlen($buff) > $n_chars) {
    $cut_index = strpos($buff, ' ', $n_chars);
    $buff = substr($buff, 0, ($cut_index === false ? $n_chars : $cut_index + 1)) . $crop_str;
    // then cut for last newline or dot
    $cut_index_a = strpos($buff, '.', $n_chars);
    $cut_index_b = strpos($buff, PHP_EOL, $n_chars);
    $cut_index = max((int) $cut_index_a, (int) $cut_index_b);
    $buff = substr($buff, 0, ($cut_index === false ? $n_chars : $cut_index + 1)) . $crop_str;
  }
  return trim($buff);
}

function formatdsc($description)
{
  global $pandoc;
  global $config;
  $description = strip_tags($description, ['img','p','br','b','blockquote','i','strong','em','h4','h5','h6','s','u','abbr','hr','ul','li','ol',]);
  $format = 'plain';
  if ($config['AP_MARKDOWN'] === true) {
    $format = 'commonmark';
  }
  $description = $pandoc->runWith($description, [
    'from' => 'html',
    'to' => $format,
    'wrap' => 'none',
  ]);
  $description = str_replace('Читать дальше', '', $description);
  $description = trim($description);
  $description = str_replace('<img src="/', '<img src="https://ifhub.club/', $description);
  return $description;
}

function download($url, $outFile)
{
  $options = array(
    CURLOPT_FILE    => fopen($outFile, 'w'),
    CURLOPT_TIMEOUT =>  28800, // set this to 8 hours so we dont timeout on big files
    CURLOPT_URL     => $url
  );

  $ch = curl_init();
  curl_setopt_array($ch, $options);
  curl_exec($ch);
  curl_close($ch);
}

$string = get_text('https://ifhub.club/rss/full/');
$service = new \Sabre\Xml\Service();
$service->elementMap = [
  '{}item' => function (\Sabre\Xml\Reader $reader) {
    return \Sabre\Xml\Deserializer\keyValue($reader, '');
  },
  '{}channel' => function (\Sabre\Xml\Reader $reader) {
    return \Sabre\Xml\Deserializer\repeatingElements($reader, '{}item');
  },
];
if (empty($string)) {
  echo 'No value to parse';
  return;
}
$articles = $service->parse($string);
unset($string);
if (empty($articles)) {
  echo 'No articles';
  return;
}
$articles = $articles[0]['value'];
$pandoc = new \Pandoc\Pandoc();

$articles = array_reverse($articles);
foreach ($articles as $article) {
  if (is_array($seen) && in_array($article['link'], $seen)) {
    echo $article['title']." - skipped\n";
    continue;
  }
  $title = $article['title'];
  $link_mastodon = $article['link'];
  $description = $article['description'];
  $image = NULL;
  preg_match('/<img.+src=[\'"](?P<src>.+?)[\'"].*>/i', $description, $image);
  if (isset($image[1])) {
    $image = $image[1];
  }
  $long_description = $description;
  $cut = strpos($description, '<a name="cut"');
  if ($cut > 0) {
    $description = substr($description, 0, strpos($description, '<a name="cut"'));
  }
  $description = formatdsc($description);
  /*
  if (strlen($description) < 50) { // description is too small
    $description = $long_description;
    $description = formatdsc($description);
  }
   */
  $limit = $config['AP_CHAR_LIMIT'] - strlen($link_mastodon) - strlen($title) - 20;
  $title = trim($title);
  $author = $article['{http://purl.org/dc/elements/1.1/}creator'];
  if ($config['AP_MARKDOWN'] === true) {
    // description is Markdown
    $description_telegram = "*$title* ($author)\n\n" . ellipse($description, $limit);
    $description_mastodon = "#### $title\n*$author*\n\n" . ellipse($description, $limit);
  } else {
    // plaintext
    $description_telegram = "*$title* ($author)\n\n> " . ellipse($description, $limit);
    $description_mastodon = "$title ($author)\n\n" . ellipse($description, $limit);
  }
  $description_telegram = $description_telegram . "\n\nЧитать дальше: $link_mastodon";
  $tag = $article['category'];
  $description_mastodon = $description_mastodon . "\n\nЧитать дальше: $link_mastodon\n#intfiction #".$tag;

  if (!$config['DRY_RUN']) {
    if ($config['TELEGRAM'] === true) {
      try {
        $telegram = new Longman\TelegramBot\Telegram(
          $config['TELEGRAM_API_KEY'],
          $config['TELEGRAM_BOT_NAME']
        );
      } catch (\Exception $e) {
        if (!$config['DRY_RUN']) {
          echo 'Wrong Telegram API key.';
          return;
        };
      }
      try {
        $result = \Longman\TelegramBot\Request::sendMessage([
          'chat_id' => $config['TELEGRAM_CHAT_ID'],
          'text' => $description_telegram,
          'parse_mode' => 'Markdown'
        ]);
        if (!$config['DRY_RUN']) {
          file_put_contents('.lastrun', time(), LOCK_EX);
        }
      } catch (Longman\TelegramBot\Exception\TelegramException $e) {
        shell_exec('echo "could not post to telegram: ' . $e->getMessage() . '" | apprise');
      }
    }
    if ($config['ACTIVITYPUB'] === true) {
      /*
      if ($image) {
        download('https://ifhub.club'.$image, './'.basename($image));
        $attachment = $mastodon->post('/media', [
          'file' => file_get_contents('./'.basename($image))
        ]);
        var_dump($attachment);
        unlink('./'.basename($image));
        $mdescription .= $attachment->url;
      }*/
      try {
        mastodon_post($description_mastodon);
      } catch (\Exception $e) {
        echo $e->getMessage();
        shell_exec('echo "could not post to mastodon: ' . $e->getMessage() . '" | apprise');
      }
    }
    if (!$config['DRY_RUN'] && !empty($config['INDEXNOW'])) {
      $client = new \GuzzleHttp\Client();
      $client->request('GET', 'https://www.bing.com/indexnow?url=' . $article['link'] . '&key=' . $config['INDEXNOW']);
    }
  } else {
    echo $description . "\n---\n";
  }
  $seen[] = $article['link'];
}
file_put_contents('.seen.yml', Yaml::dump($seen), LOCK_EX);
